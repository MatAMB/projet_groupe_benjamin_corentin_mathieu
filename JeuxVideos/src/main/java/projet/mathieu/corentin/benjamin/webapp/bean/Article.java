package projet.mathieu.corentin.benjamin.webapp.bean;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Lob;

import org.hibernate.annotations.Type;

@Entity
public class Article {
	@Id
	@GeneratedValue
	private Long id;

	private String titre;
	private String type;
	private String auteur;

	public Article(Long id, String titre, String type, String auteur, String contenu) {
		super();
		this.id = id;
		this.titre = titre;
		this.type = type;
		this.auteur = auteur;
		this.contenu = contenu;
	}

	public String getTitre() {
		return titre;
	}

	public void setTitre(String titre) {
		this.titre = titre;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getAuteur() {
		return auteur;
	}

	public void setAuteur(String auteur) {
		this.auteur = auteur;
	}

	@Lob
	@Type(type = "org.hibernate.type.TextType")
	private String contenu;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getContenu() {
		return contenu;
	}

	public void setContenu(String contenu) {
		this.contenu = contenu;
	}

	public Article(String contenu) {
		super();

		this.contenu = contenu;
	}

	public Article() {

	}
}
