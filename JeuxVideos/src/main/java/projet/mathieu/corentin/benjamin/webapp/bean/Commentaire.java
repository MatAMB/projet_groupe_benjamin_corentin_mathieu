package projet.mathieu.corentin.benjamin.webapp.bean;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Commentaire {

	@Id
	@GeneratedValue
	private Long id;

	private String com;
	private String auteur;

	public String getAuteur() {
		return auteur;
	}

	public void setAuteur(String auteur) {
		this.auteur = auteur;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCom() {
		return com;
	}

	public void setCom(String com) {
		this.com = com;
	}

	public Commentaire(Long id, String com, String auteur) {
		super();
		this.id = id;
		this.com = com;
		this.auteur = auteur;
	}

	public Commentaire() {

	}

}
