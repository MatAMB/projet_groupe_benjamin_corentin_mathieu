package projet.mathieu.corentin.benjamin.webapp.bean;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Jeux {
	
	@Id
	@GeneratedValue
	private Long id;
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}



	private String nom;
	private String sortie;
	private String description;
	private String plateforme;
	private String  genre;
	private String classification;
	
	
	
	public Jeux() {
		
	}

	public Jeux(String nom, String sortie, String description, String plateforme, String genre, String classification) {
		super();
		this.nom = nom;
		this.sortie = sortie;
		this.description = description;
		this.plateforme = plateforme;
		this.genre = genre;
		this.classification = classification;
	}



	public String getNom() {
		return nom;
	}



	public void setNom(String nom) {
		this.nom = nom;
	}



	public String getSortie() {
		return sortie;
	}



	public void setSortie(String sortie) {
		this.sortie = sortie;
	}



	public String getDescription() {
		return description;
	}



	public void setDescription(String description) {
		this.description = description;
	}



	public String getPlateforme() {
		return plateforme;
	}



	public void setPlateforme(String plateforme) {
		this.plateforme = plateforme;
	}



	public String getGenre() {
		return genre;
	}



	public void setGenre(String genre) {
		this.genre = genre;
	}



	public String getClassification() {
		return classification;
	}



	public void setClassification(String classification) {
		this.classification = classification;
	}
	
	
	
	
}

