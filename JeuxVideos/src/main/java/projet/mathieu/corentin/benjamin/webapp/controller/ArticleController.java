package projet.mathieu.corentin.benjamin.webapp.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import projet.mathieu.corentin.benjamin.webapp.bean.Article;
import projet.mathieu.corentin.benjamin.webapp.dao.ArticleDao;

@Controller
public class ArticleController {

	@Autowired
	private ArticleDao articleDao;

	@GetMapping("creer_article")
	public String afficherCreerArticle() {
		return "creer_article";
	}

	@PostMapping("creation-article")
	public String doCreerArticle(String contenu) {

		Article a = new Article(contenu);
		articleDao.save(a);
		return "redirect:afficher_articles";
	}

	@GetMapping("afficher_article")
	public String afficherArticle(Model model, Long id) {
		model.addAttribute("articleIhm", articleDao.getOne(id));
		return "afficher_article";
	}

	@GetMapping("afficher_articles")
	public String afficherArticles(Model model) {
		List<Article> articles = articleDao.findAll();
		model.addAttribute("articlesIhm", articles);
		return "afficher_articles";
	}

}
