package projet.mathieu.corentin.benjamin.webapp.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class CatalogueController {

	@RequestMapping(method = RequestMethod.GET, value = "catalogue")
	public String affichageCatalogue() {

		return "catalogue";

	}

	@RequestMapping(method = RequestMethod.GET, value = { "accueil", "/" })
	public String affichageAccueil() {

		return "Accueil";

	}

	@RequestMapping(method = RequestMethod.GET, value = "nav")
	public String affichageNavbar() {

		return "Narvbar";
	}

	@RequestMapping(method = RequestMethod.GET, value = "presentation")
	public String affichagePresentation() {

		return "presentation";
	}

	@RequestMapping(method = RequestMethod.GET, value = "menu")
	public String affichageMenu() {

		return "Menu";
	}

	@RequestMapping(method = RequestMethod.GET, value = "menu2")
	public String affichageMenu2() {

		return "menu2";
	}

	

}