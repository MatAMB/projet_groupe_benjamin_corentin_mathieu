package projet.mathieu.corentin.benjamin.webapp.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import projet.mathieu.corentin.benjamin.webapp.bean.Article;
import projet.mathieu.corentin.benjamin.webapp.bean.Utilisateur;
import projet.mathieu.corentin.benjamin.webapp.dao.UtilisateurDao;

@Controller
public class UtilisateurController {

	@Autowired
	private UtilisateurDao utiDao;
	
	private static List<Utilisateur> utilisateurs = new ArrayList<Utilisateur>();
	static {
		utilisateurs.add(new Utilisateur(1l, "DELBOIS", "Corentin", "10/6 Allé Desmond Tutu, 59150, wattrelos",
				new Date(), "0635661847", "delboiscorentin@gmail.com", "Lancelot59150"));
		utilisateurs.add(new Utilisateur(2l, "LISSON", "Benjamin", "27 rue godefroy, 59000, Lille", new Date(),
				"0616244845", "benji-1996@hotmail.fr", "Leffemoi"));
		utilisateurs.add(new Utilisateur(3l, "Amb", "Mat", "10 rue du babar, 59803, lille3", new Date(), "0658547453",
				"mat.ambroise@gmail.com", "Azerty1233"));
	}

	@RequestMapping(method = RequestMethod.GET, value = "deconnexion")
	public String doDeconnexion(HttpSession session) {
		session.invalidate();
		return "redirect:accueil";
	}

	@RequestMapping(method = RequestMethod.POST, value = "login")
	public String doConnexion(HttpSession session, RedirectAttributes redir, @RequestParam("login") String login,
			@RequestParam("password") String password) {

		for (Utilisateur utilisateur : utilisateurs) {
			if (utilisateur.getLogin().equalsIgnoreCase(login) && utilisateur.getPassword().equals(password)) {
				session.setAttribute("connectedUser", utilisateur);
				return "redirect:accueil";
			}
		}

		redir.addFlashAttribute("messageErr", "Login/password invalid");

		return "redirect:inscription2";
	}

	@RequestMapping(method = RequestMethod.GET, value = "connexion")
	public String affichageConnexion() {

		return "connexion";
	}

//	@RequestMapping(method = RequestMethod.GET, value = "inscription")
//	public String affichageInscription() {
//
//		return "inscription";
//	}
	@RequestMapping(method = RequestMethod.GET, value = "inscription2")
	public String affichageInscription2() {

		return "inscription2";
	}
	

	@RequestMapping(value = "do_creation_utilisateur", method = RequestMethod.POST)
	private String doCreationUtilisateur(Model model, Utilisateur nouveauUtilisateur) {
		utiDao.save(nouveauUtilisateur);
		model.addAttribute("newUser", nouveauUtilisateur);
		return "accueil";
}
	@GetMapping("afficher_utilisateurs")
	public String afficherArticles(Model model) {
		List<Utilisateur> utilisateurs = utiDao.findAll();
		model.addAttribute("utiIhm", utilisateurs);
		return "afficher_utilisateurs";
	}
}