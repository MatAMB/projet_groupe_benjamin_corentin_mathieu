package projet.mathieu.corentin.benjamin.webapp.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import projet.mathieu.corentin.benjamin.webapp.bean.Article;

@Repository
public interface ArticleDao extends JpaRepository<Article, Long> {

}
