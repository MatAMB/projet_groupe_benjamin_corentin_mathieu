package projet.mathieu.corentin.benjamin.webapp.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import projet.mathieu.corentin.benjamin.webapp.bean.Utilisateur;

@Repository
public interface UtilisateurDao extends JpaRepository<Utilisateur, Long> {

	public Utilisateur findByEmailAndPassword(String email,String password);
	
}
